# Simple LXDE Slideshow wallpaper

- change 'folder' to your wallpaper path
- change 'seconds' to your desired transition time
- add script on startup

Supports jpg,jpeg,png,gif files

Based on [MisterPixels script](https://gist.github.com/MisterPixels/04cb30f112dbd257cbc25f2f757cb797) but without random wallpaper selection, it creates a random permutation of the files in the folder