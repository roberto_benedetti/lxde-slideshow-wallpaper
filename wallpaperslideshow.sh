#!/bin/bash

folder='/home/rba/Pictures/lolpapers' #folder containing the images
seconds=200	 #transition time in secs
while(true)
	do
	ls "$folder" |sort -R | while read filename;
		do
	#		echo $filename
		if [ `echo $filename | grep .png` ] || [ `echo $filename | grep .jpg` ] || [ `echo $filename | grep .jpeg` ] || [ `echo $filename | grep .gif` ] # if the file chosen is a jpg, jpeg, png or gif 
		then
			pcmanfm --set-wallpaper="$folder/$filename" # set the wallpaper to the random file chosen
			sleep $seconds # sleep for amount of seconds given by the user
		fi
	done
done
